package com.bcp.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.bcp.challenge.delegate.impl.UserDelegateImpl;
import com.bcp.challenge.dto.UserDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import org.junit.jupiter.api.Test;

@AutoConfigureMockMvc
@ContextConfiguration(classes = { UserController.class })
@WebMvcTest
class UserControllerTest {

	@MockBean
	private UserDelegateImpl userDelegate;

	@Autowired
	private MockMvc mvc;

	@Test
	void whenGetAllUsers() throws Exception {

//		List<UserDTO> listUser = new ArrayList<UserDTO>();
//		UserDTO user = new UserDTO();
//		user.setUserId(1);
//		user.setDni(8888881);
//		user.setFirstName("Name 1");
//		user.setLastName1("Last Name 1-1");
//		user.setLastName2("Last Name 1-2");
//
//		listUser.add(user);
//
//		user = new UserDTO();
//		user.setUserId(2);
//		user.setDni(8888882);
//		user.setFirstName("Name 2");
//		user.setLastName1("Last Name 2-1");
//		user.setLastName2("Last Name 2-2");
//		listUser.add(user);

//		Mockito.when(userDelegate.getAllUsers()).thenReturn(listUser);

		mvc.perform(get("/user-api/get-all-user").contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());

	}

	@Test
	void whenGetUser() throws Exception {
		
		mvc.perform(get("/user-api/get-user/" + 1).contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	void whenUpdateUser() throws Exception {

		UserDTO request = new UserDTO();
		request.setUserId(1);
		request.setDni(8888881);
		request.setFirstName("Name 1");
		request.setLastName1("Last Name 1-1");
		request.setLastName2("Last Name 1-2");

		ObjectMapper mapper = new ObjectMapper();
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(request);

		mvc.perform(put("/user-api/update-user/" + request.getUserId()).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(requestJson))
		.andExpect(status().isOk());

	}
	
	@Test
	void whenDeleteUser() throws Exception {
		mvc.perform(delete("/user-api/delete-user/" + 1).contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());

	}
	
	@Test
	void whenCreateUser() throws Exception {
		UserDTO request = new UserDTO();
		request.setUserId(1);
		request.setDni(8888881);
		request.setFirstName("Name 1");
		request.setLastName1("Last Name 1-1");
		request.setLastName2("Last Name 1-2");

		ObjectMapper mapper = new ObjectMapper();
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(request);

		mvc.perform(post("/user-api/create-user").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(requestJson))
		.andExpect(status().isOk());

	}

}
