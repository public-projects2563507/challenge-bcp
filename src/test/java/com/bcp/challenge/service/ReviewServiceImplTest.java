package com.bcp.challenge.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bcp.challenge.dto.ReviewDTO;
import com.bcp.challenge.mapper.ReviewMapper;
import com.bcp.challenge.repository.IReviewRepository;
import com.bcp.challenge.service.impl.ReviewServiceImpl;

@ExtendWith(MockitoExtension.class)
public class ReviewServiceImplTest {


	private ReviewServiceImpl reviewService;
	
	@Mock
    private IReviewRepository reviewRepository;
	
	@BeforeEach
    void setUp() {
		reviewService = new ReviewServiceImpl(reviewRepository);
    }
	
	@Test
	void whenGetAllReview() throws Exception {
				
		reviewService.getAllReviews();

	}
	
	@Test
	void whenGetReview() throws Exception {
		
		ReviewDTO request = new ReviewDTO();
		request.setReviewId(1);
		request.setComment("comment-001-4.9");
		request.setRating(4.9);
		request.setItemId(1);
		request.setUserId(1);
		
		reviewService.getReview(1);


	}
	
	@Test
	void whenUpdateReview() throws Exception {
		
		ReviewDTO request = new ReviewDTO();
		request.setReviewId(1);
		request.setComment("comment-001-4.9");
		request.setRating(4.9);
		request.setItemId(1);
		request.setUserId(1);
		
		reviewService.saveReview(ReviewMapper.getObjectEntity(request));

	}

	@Test
	void whenCreateReview() throws Exception {
		
		ReviewDTO request = new ReviewDTO();
		request.setReviewId(1);
		request.setComment("comment-001-4.9");
		request.setRating(4.9);
		request.setItemId(1);
		request.setUserId(1);
		
		reviewService.saveReview(ReviewMapper.getObjectEntity(request));


	}
	
	@Test
	void whenDeleteReview() throws Exception {
		
		ReviewDTO request = new ReviewDTO();
		request.setReviewId(1);
		request.setComment("comment-001-4.9");
		request.setRating(4.9);
		request.setItemId(1);
		request.setUserId(1);
		
		reviewService.deleteReview(request.getReviewId());


	}

}
