package com.bcp.challenge.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bcp.challenge.dto.UserDTO;
import com.bcp.challenge.mapper.UserMapper;
import com.bcp.challenge.repository.IUserRepository;
import com.bcp.challenge.service.impl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {


	private UserServiceImpl userService;
	
	@Mock
    private IUserRepository userRepository;
	
	@BeforeEach
    void setUp() {
		userService = new UserServiceImpl(userRepository);
    }
	
	@Test
	void whenGetAllUser() throws Exception {
				
		userService.getAllUsers();

	}
	
	@Test
	void whenGetUser() throws Exception {
		
		UserDTO user = new UserDTO();
		user.setUserId(2);
		user.setDni(8888882);
		user.setFirstName("Name 2");
		user.setLastName1("Last Name 2-1");
		user.setLastName2("Last Name 2-2");
		
		userService.getUser(1);


	}
	
	@Test
	void whenUpdateUser() throws Exception {
		
		UserDTO user = new UserDTO();
		user.setUserId(2);
		user.setDni(8888882);
		user.setFirstName("Name 2");
		user.setLastName1("Last Name 2-1");
		user.setLastName2("Last Name 2-2");
		
		userService.saveUser(UserMapper.getObjectEntity(user));

	}

	@Test
	void whenCreateUser() throws Exception {
		
		UserDTO user = new UserDTO();
		user.setUserId(2);
		user.setDni(8888882);
		user.setFirstName("Name 2");
		user.setLastName1("Last Name 2-1");
		user.setLastName2("Last Name 2-2");
		
		userService.saveUser(UserMapper.getObjectEntity(user));


	}
	
	@Test
	void whenDeleteUser() throws Exception {
		
		UserDTO user = new UserDTO();
		user.setUserId(2);
		user.setDni(8888882);
		user.setFirstName("Name 2");
		user.setLastName1("Last Name 2-1");
		user.setLastName2("Last Name 2-2");
		
		userService.deleteUser(user.getUserId());


	}

}
