package com.bcp.challenge.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.bcp.challenge.dto.ItemDTO;
import com.bcp.challenge.mapper.ItemMapper;
import com.bcp.challenge.repository.IItemRepository;
import com.bcp.challenge.service.impl.ItemServiceImpl;

@ExtendWith(MockitoExtension.class)
public class ItemServiceImplTest {


	private ItemServiceImpl itemService;
	
	@Mock
    private IItemRepository itemRepository;
	
	@BeforeEach
    void setUp() {
		itemService = new ItemServiceImpl(itemRepository);
    }
	
	@Test
	void whenGetAllItem() throws Exception {
				
		itemService.getAllItems();

	}
	
	@Test
	void whenGetItem() throws Exception {
		
		ItemDTO request = new ItemDTO();
		request.setItemId(1);
		request.setCode("001");
		request.setTitle("title-001");
		request.setName("name-001");
		request.setDescription("description-001");
		
		itemService.getItem(1);


	}
	
	@Test
	void whenUpdateItem() throws Exception {
		
		ItemDTO request = new ItemDTO();
		request.setItemId(1);
		request.setCode("001");
		request.setTitle("title-001");
		request.setName("name-001");
		request.setDescription("description-001");
		
		itemService.saveItem(ItemMapper.getObjectEntity(request));

	}

	@Test
	void whenCreateItem() throws Exception {
		
		ItemDTO request = new ItemDTO();
		request.setItemId(1);
		request.setCode("001");
		request.setTitle("title-001");
		request.setName("name-001");
		request.setDescription("description-001");
		
		itemService.saveItem(ItemMapper.getObjectEntity(request));


	}
	
	@Test
	void whenDeleteItem() throws Exception {
		
		ItemDTO request = new ItemDTO();
		request.setItemId(1);
		request.setCode("001");
		request.setTitle("title-001");
		request.setName("name-001");
		request.setDescription("description-001");
		
		itemService.deleteItem(request.getItemId());


	}
	
	@Test
	void whenGetTitle() throws Exception {

		itemService.getTitles(4.0);

	}
	
	@Test
	void whenGetAverageRatingLowerThan() throws Exception {

		itemService.getAverageRatingLowerThan(4.6);

	}

}
