package com.bcp.challenge.delegate.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.bcp.challenge.delegate.IReviewDelegate;
import com.bcp.challenge.dto.ReviewDTO;
import com.bcp.challenge.entity.Review;
import com.bcp.challenge.mapper.GenericResponse;
import com.bcp.challenge.mapper.ReviewMapper;
import com.bcp.challenge.service.IReviewService;

@Service
public class ReviewDelegateImpl implements IReviewDelegate {

	private final IReviewService reviewService;

	public ReviewDelegateImpl(IReviewService reviewService) {
		this.reviewService = reviewService;
	}

	@Override
	public List<ReviewDTO> getAllReviews() {

		return ReviewMapper.getListObject(reviewService.getAllReviews());
	}

	@Override
	public ReviewDTO getReview(Integer id) {

		Optional<Review> object = reviewService.getReview(id);

		if (object.isEmpty()) {

		}

		return ReviewMapper.getObjectDTO(object.get());

	}

	@Override
	public GenericResponse deleteReview(Integer id) {

		GenericResponse response = new GenericResponse();
		reviewService.deleteReview(id);

		response.setCode("00");
		response.setSuccess(true);
		response.setMessageResponse("object delete successfull");
		response.setTechnicalMessageResponse("OK");

		return response;
	}

	@Override
	public GenericResponse createReview(ReviewDTO review) {

		GenericResponse response = new GenericResponse();

		Optional<Review> object = reviewService.getReview(review.getReviewId());

		if (object.isEmpty()) {

			reviewService.saveReview(ReviewMapper.getObjectEntity(review));
			response.setCode("00");
			response.setSuccess(true);
			response.setMessageResponse("object update successfull");
			response.setTechnicalMessageResponse("OK");

		} else {
			response.setCode("01");
			response.setSuccess(false);
			response.setMessageResponse("the object already exist with id " + review.getReviewId());
			response.setTechnicalMessageResponse("FAIL");
		}

		return response;

	}

	@Override
	public GenericResponse updateReview(ReviewDTO review, Integer id) {

		GenericResponse response = new GenericResponse();

		Optional<Review> object = reviewService.getReview(id);

		if (object.isEmpty()) {
			response.setCode("01");
			response.setSuccess(false);
			response.setMessageResponse("the object not exist");
			response.setTechnicalMessageResponse("FAIL");
		} else {
			reviewService.saveReview(ReviewMapper.getObjectEntity(review));
			response.setCode("00");
			response.setSuccess(true);
			response.setMessageResponse("object update successfull");
			response.setTechnicalMessageResponse("OK");
		}

		return response;

	}

}
