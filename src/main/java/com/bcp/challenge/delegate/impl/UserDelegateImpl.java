package com.bcp.challenge.delegate.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.bcp.challenge.delegate.IUserDelegate;
import com.bcp.challenge.dto.UserDTO;
import com.bcp.challenge.entity.User;
import com.bcp.challenge.mapper.GenericResponse;
import com.bcp.challenge.mapper.UserMapper;
import com.bcp.challenge.service.IUserService;

@Service
public class UserDelegateImpl implements IUserDelegate {

	private final IUserService userService;

	public UserDelegateImpl(IUserService userService) {
		this.userService = userService;
	}

	@Override
	public List<UserDTO> getAllUsers() {

		return UserMapper.getListObject(userService.getAllUsers());
	}

	@Override
	public UserDTO getUser(Integer id) {

		Optional<User> object = userService.getUser(id);

		if (object.isEmpty()) {

		}

		return UserMapper.getObjectDTO(object.get());

	}

	@Override
	public GenericResponse deleteUser(Integer id) {

		GenericResponse response = new GenericResponse();
		userService.deleteUser(id);

		response.setCode("00");
		response.setSuccess(true);
		response.setMessageResponse("object delete successfull");
		response.setTechnicalMessageResponse("OK");

		return response;
	}

	@Override
	public GenericResponse createUser(UserDTO user) {

		GenericResponse response = new GenericResponse();

		Optional<User> object = userService.getUser(user.getUserId());

		if (object.isEmpty()) {

			userService.saveUser(UserMapper.getObjectEntity(user));
			response.setCode("00");
			response.setSuccess(true);
			response.setMessageResponse("object create successfull");
			response.setTechnicalMessageResponse("OK");

		} else {
			response.setCode("01");
			response.setSuccess(false);
			response.setMessageResponse("the object already exist with id " + user.getUserId());
			response.setTechnicalMessageResponse("FAIL");
		}

		return response;

	}

	@Override
	public GenericResponse updateUser(UserDTO user, Integer id) {

		GenericResponse response = new GenericResponse();

		Optional<User> object = userService.getUser(id);

		if (object.isEmpty()) {
			response.setCode("01");
			response.setSuccess(false);
			response.setMessageResponse("the object not exist");
			response.setTechnicalMessageResponse("FAIL");
		} else {
			userService.saveUser(UserMapper.getObjectEntity(user));
			response.setCode("00");
			response.setSuccess(true);
			response.setMessageResponse("object update successfull");
			response.setTechnicalMessageResponse("OK");
		}

		return response;

	}

}
