package com.bcp.challenge.delegate.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.bcp.challenge.delegate.IItemDelegate;
import com.bcp.challenge.dto.ItemDTO;
import com.bcp.challenge.entity.Item;
import com.bcp.challenge.mapper.AverageRatingLowerThanResponse;
import com.bcp.challenge.mapper.GenericResponse;
import com.bcp.challenge.mapper.ItemMapper;
import com.bcp.challenge.service.IItemService;

@Service
public class ItemDelegateImpl implements IItemDelegate {

	private final IItemService itemService;

	public ItemDelegateImpl(IItemService itemService) {
		this.itemService = itemService;
	}

	@Override
	public List<ItemDTO> getAllItems() {

		return ItemMapper.getListObject(itemService.getAllItems());
	}

	@Override
	public ItemDTO getItem(Integer id) {

		Optional<Item> object = itemService.getItem(id);

		if (object.isEmpty()) {

		}

		return ItemMapper.getObjectDTO(object.get());

	}

	@Override
	public GenericResponse deleteItem(Integer id) {

		GenericResponse response = new GenericResponse();
		itemService.deleteItem(id);

		response.setCode("00");
		response.setSuccess(true);
		response.setMessageResponse("object delete successfull");
		response.setTechnicalMessageResponse("OK");

		return response;
	}

	@Override
	public GenericResponse createItem(ItemDTO item) {

		GenericResponse response = new GenericResponse();

		Optional<Item> object = itemService.getItem(item.getItemId());

		if (object.isEmpty()) {

			itemService.saveItem(ItemMapper.getObjectEntity(item));
			response.setCode("00");
			response.setSuccess(true);
			response.setMessageResponse("object update successfull");
			response.setTechnicalMessageResponse("OK");

		} else {
			response.setCode("01");
			response.setSuccess(false);
			response.setMessageResponse("the object already exist with id " + item.getItemId());
			response.setTechnicalMessageResponse("FAIL");
		}

		return response;

	}

	@Override
	public GenericResponse updateItem(ItemDTO item, Integer id) {

		GenericResponse response = new GenericResponse();

		Optional<Item> object = itemService.getItem(id);

		if (object.isEmpty()) {
			response.setCode("01");
			response.setSuccess(false);
			response.setMessageResponse("the object not exist");
			response.setTechnicalMessageResponse("FAIL");
		} else {
			itemService.saveItem(ItemMapper.getObjectEntity(item));
			response.setCode("00");
			response.setSuccess(true);
			response.setMessageResponse("object update successfull");
			response.setTechnicalMessageResponse("OK");
		}

		return response;

	}

	@Override
	public List<String> getTitles(Double rating) {

		return itemService.getTitles(rating);

	}

	@Override
	public AverageRatingLowerThanResponse getAverageRatingLowerThan(Double rating) {

		Double average = 0D;
		AverageRatingLowerThanResponse response = new AverageRatingLowerThanResponse();
		List<Item> listItem = itemService.getAverageRatingLowerThan(rating);
		List<Double> listRating = new ArrayList<>();

		response.setSuccess(true);
		response.setCode("00");
		response.setMessageResponse("Average succesfull");

		if (listItem.size() > 0) {

			listItem = listItem.stream().distinct().collect(Collectors.toList());

			for (Item item : listItem) {
				item.getReviews().stream()
						.filter(filterItemId -> filterItemId.getItem().getItemId() == item.getItemId())
						.filter(filterRating -> filterRating.getRating() < rating)
						.map(mapRating -> mapRating.getRating())
						.forEach(valueRating -> listRating.add(valueRating));

			}

			average = listRating.stream().reduce((double) 0, Double::sum) / listRating.stream().count();
			response.setTechnicalMessageResponse("Exist rating");
			
		} else {
			response.setTechnicalMessageResponse("Not exist rating");
		}

		response.setAverageRating(average);

		return response;

	}

}
