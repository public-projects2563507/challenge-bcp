package com.bcp.challenge.delegate;

import java.util.List;

import com.bcp.challenge.dto.UserDTO;
import com.bcp.challenge.mapper.GenericResponse;

public interface IUserDelegate {

	public List<UserDTO> getAllUsers();
	
	public UserDTO getUser(Integer id);
	
	public GenericResponse deleteUser(Integer id);
	
	public GenericResponse createUser(UserDTO user);

	public GenericResponse updateUser(UserDTO user, Integer id);

}
