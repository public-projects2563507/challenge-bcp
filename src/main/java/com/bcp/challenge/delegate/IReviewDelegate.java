package com.bcp.challenge.delegate;

import java.util.List;

import com.bcp.challenge.dto.ReviewDTO;
import com.bcp.challenge.mapper.GenericResponse;

public interface IReviewDelegate {

	public List<ReviewDTO> getAllReviews();
	
	public ReviewDTO getReview(Integer id);
	
	public GenericResponse deleteReview(Integer id);
	
	public GenericResponse createReview(ReviewDTO review);

	public GenericResponse updateReview(ReviewDTO review, Integer id);

}
