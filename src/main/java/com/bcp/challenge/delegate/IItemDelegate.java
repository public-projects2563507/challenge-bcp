package com.bcp.challenge.delegate;

import java.util.List;

import com.bcp.challenge.dto.ItemDTO;
import com.bcp.challenge.mapper.AverageRatingLowerThanResponse;
import com.bcp.challenge.mapper.GenericResponse;

public interface IItemDelegate {

	public List<ItemDTO> getAllItems();

	public ItemDTO getItem(Integer id);

	public GenericResponse deleteItem(Integer id);

	public GenericResponse createItem(ItemDTO review);

	public GenericResponse updateItem(ItemDTO review, Integer id);

	public List<String> getTitles(Double rating);

	public AverageRatingLowerThanResponse getAverageRatingLowerThan(Double rating);

}
