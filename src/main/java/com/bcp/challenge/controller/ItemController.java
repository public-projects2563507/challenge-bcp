package com.bcp.challenge.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;

import com.bcp.challenge.delegate.IItemDelegate;
import com.bcp.challenge.dto.ItemDTO;
import com.bcp.challenge.mapper.AverageRatingLowerThanResponse;
import com.bcp.challenge.mapper.GenericResponse;

@RestController
@RequestMapping("/item-api")
public class ItemController {

	private final IItemDelegate itemDelegate;

	public ItemController(IItemDelegate itemDelegate) {
		this.itemDelegate = itemDelegate;
	}

	@RequestMapping(value = {
			"/get-all-item" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ItemDTO>> getAllItems() throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(itemDelegate.getAllItems());
	}

	@RequestMapping(value = {
			"/get-item/{id}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ItemDTO> getItem(@PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(itemDelegate.getItem(id));
	}

	@RequestMapping(value = {
			"/delete-item/{id}" }, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> deleteItem(@PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(itemDelegate.deleteItem(id));
	}

	@RequestMapping(value = {
			"/create-item" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> createItem(@RequestBody ItemDTO item) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(itemDelegate.createItem(item));
	}

	@RequestMapping(value = { "/update-item/{id}" }, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> updateItem(@RequestBody ItemDTO item, @PathVariable Integer id)
			throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(itemDelegate.updateItem(item, id));
	}

	@RequestMapping(value = { "/titles/{rating}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> getTitles(@PathVariable Double rating) throws Exception {
		return ResponseEntity.status(HttpStatus.OK).body(itemDelegate.getTitles(rating));
	}

	@RequestMapping(value = {
			"/average-rating-lower-than/{rating}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AverageRatingLowerThanResponse> getAverageRatingLowerThan(@PathVariable Double rating) throws Exception {
		return ResponseEntity.status(HttpStatus.OK).body(itemDelegate.getAverageRatingLowerThan(rating));
	}

}
