package com.bcp.challenge.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;

import com.bcp.challenge.delegate.IUserDelegate;
import com.bcp.challenge.dto.UserDTO;
import com.bcp.challenge.mapper.GenericResponse;

@RestController
@RequestMapping("/user-api")
public class UserController {

	private final IUserDelegate userDelegate;

	public UserController(IUserDelegate userDelegate) {
		this.userDelegate = userDelegate;
	}

	@RequestMapping(value = {
			"/get-all-user" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserDTO>> getAllUsers() throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(userDelegate.getAllUsers());
	}

	@RequestMapping(value = {
			"/get-user/{id}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> getUser(@PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(userDelegate.getUser(id));
	}

	@RequestMapping(value = {
			"/delete-user/{id}" }, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> deleteUser(@PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(userDelegate.deleteUser(id));
	}

	@RequestMapping(value = {
			"/create-user" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> createUser(@RequestBody UserDTO user) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(userDelegate.createUser(user));
	}

	@RequestMapping(value = { "/update-user/{id}" }, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> updateUser(@RequestBody UserDTO user, @PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(userDelegate.updateUser(user, id));
	}

}
