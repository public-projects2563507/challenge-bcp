package com.bcp.challenge.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;

import com.bcp.challenge.delegate.IReviewDelegate;
import com.bcp.challenge.dto.ReviewDTO;
import com.bcp.challenge.mapper.GenericResponse;

@RestController
@RequestMapping("/review-api")
public class ReviewController {

	private final IReviewDelegate reviewDelegate;

	public ReviewController(IReviewDelegate reviewDelegate) {
		this.reviewDelegate = reviewDelegate;
	}

	@RequestMapping(value = {
			"/get-all-review" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ReviewDTO>> getAllReviews() throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(reviewDelegate.getAllReviews());
	}

	@RequestMapping(value = {
			"/get-review/{id}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ReviewDTO> getReview(@PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(reviewDelegate.getReview(id));
	}

	@RequestMapping(value = {
			"/delete-review/{id}" }, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> deleteReview(@PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(reviewDelegate.deleteReview(id));
	}

	@RequestMapping(value = {
			"/create-review" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> createReview(@RequestBody ReviewDTO review) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(reviewDelegate.createReview(review));
	}

	@RequestMapping(value = { "/update-review/{id}" }, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> updateReview(@RequestBody ReviewDTO review, @PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(reviewDelegate.updateReview(review, id));
	}

}
