package com.bcp.challenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bcp.challenge.entity.Review;


public interface IReviewRepository extends JpaRepository<Review, Integer>{

}
