package com.bcp.challenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bcp.challenge.entity.User;


public interface IUserRepository extends JpaRepository<User, Integer>{

}
