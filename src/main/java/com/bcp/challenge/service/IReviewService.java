package com.bcp.challenge.service;

import java.util.List;
import java.util.Optional;

import com.bcp.challenge.entity.Review;

public interface IReviewService {

	public List<Review> getAllReviews();

	public Optional<Review> getReview(Integer id);

	public void deleteReview(Integer id);

	public Review saveReview(Review review);

}