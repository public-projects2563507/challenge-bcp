package com.bcp.challenge.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.bcp.challenge.entity.User;
import com.bcp.challenge.repository.IUserRepository;
import com.bcp.challenge.service.IUserService;

@Service
public class UserServiceImpl implements IUserService {

	private IUserRepository userRepository;

	public UserServiceImpl(IUserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public Optional<User> getUser(Integer id) {
		return userRepository.findById(id);
	}

	@Override
	public void deleteUser(Integer id) {
		userRepository.deleteById(id);
	}

	@Override
	public User saveUser(User user) {
		return userRepository.save(user);
	}

}
