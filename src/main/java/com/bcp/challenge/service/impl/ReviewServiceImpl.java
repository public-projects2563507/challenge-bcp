package com.bcp.challenge.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.bcp.challenge.entity.Review;
import com.bcp.challenge.repository.IReviewRepository;
import com.bcp.challenge.service.IReviewService;

@Service
public class ReviewServiceImpl implements IReviewService {

	private IReviewRepository reviewRepository;

	public ReviewServiceImpl(IReviewRepository reviewRepository) {
		this.reviewRepository = reviewRepository;
	}

	@Override
	public List<Review> getAllReviews() {
		return reviewRepository.findAll();
	}

	@Override
	public Optional<Review> getReview(Integer id) {
		return reviewRepository.findById(id);
	}

	@Override
	public void deleteReview(Integer id) {
		reviewRepository.deleteById(id);
	}

	@Override
	public Review saveReview(Review review) {
		return reviewRepository.save(review);
	}

}
