package com.bcp.challenge.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.bcp.challenge.entity.Item;
import com.bcp.challenge.repository.IItemRepository;
import com.bcp.challenge.service.IItemService;

@Service
public class ItemServiceImpl implements IItemService {

	private IItemRepository itemRepository;

	public ItemServiceImpl(IItemRepository itemRepository) {
		this.itemRepository = itemRepository;
	}

	@Override
	public List<Item> getAllItems() {
		return itemRepository.findAll();
	}

	@Override
	public Optional<Item> getItem(Integer id) {
		return itemRepository.findById(id);
	}

	@Override
	public void deleteItem(Integer id) {
		itemRepository.deleteById(id);
	}

	@Override
	public Item saveItem(Item item) {
		return itemRepository.save(item);
	}

	@Override
	public List<String> getTitles(Double rating) {

		return itemRepository.findTitlesItemsByRating(rating);
	}

	@Override
	public List<Item> getAverageRatingLowerThan(Double rating) {
		
		return itemRepository.findItemsWithAverageRatingLowerThan(rating);

	}

}
