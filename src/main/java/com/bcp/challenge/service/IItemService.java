package com.bcp.challenge.service;

import java.util.List;
import java.util.Optional;

import com.bcp.challenge.entity.Item;

public interface IItemService {

	public List<Item> getAllItems();

	public Optional<Item> getItem(Integer id);

	public void deleteItem(Integer id);

	public Item saveItem(Item item);

	public List<String> getTitles(Double rating);

	public List<Item> getAverageRatingLowerThan(Double rating);

}
