package com.bcp.challenge.service;

import java.util.List;
import java.util.Optional;

import com.bcp.challenge.entity.User;

public interface IUserService {

	public List<User> getAllUsers();

	public Optional<User> getUser(Integer id);

	public void deleteUser(Integer id);

	public User saveUser(User user);

}