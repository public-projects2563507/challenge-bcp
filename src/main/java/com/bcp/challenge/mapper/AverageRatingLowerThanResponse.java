package com.bcp.challenge.mapper;

import lombok.Getter;
import lombok.Setter;

public class AverageRatingLowerThanResponse extends GenericResponse {

	@Setter
	@Getter
	private Double averageRating;
}
